<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';

    protected $fillable = ['ip', 'sede', 'lugar'];

    public function assist(){
        return $this->hasMany('App\Assist', 'assist_id', 'id');
    }
}
