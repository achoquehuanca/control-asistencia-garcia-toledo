<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /* Log::info('Ingresando a toArray de UserResource');
        Log::info($request); */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'nombres' => $this->nombres,
            'paterno' => $this->paterno,
            'materno' => $this->materno,
            'dni' => $this->dni,
            'email' => $this->email,
            'roles' => array_map(
                function ($role) {
                    return $role['name'];
                },
                $this->roles->toArray()
            ),
            'permissions' => array_map(
                function ($permission) {
                    return $permission['name'];
                },
                $this->getAllPermissions()->toArray()
            ),
            'avatar' => 'https://i.pravatar.cc',
        ];
    }
}
