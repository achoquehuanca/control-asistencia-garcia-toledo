<?php
/**
 * File AuthController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Laravue\JsonResponse;
use App\Assist;
use App\Device;
use App\Http\Controllers\UserSystemInfoController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers\Api
 */
class AuthController extends BaseController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        Log::info('Ingresando a login en AuthController');
        // $credentials = $request->only('email', 'password');
        $credentials = $request->only('name', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        $temp = response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
        Log::info($temp);

        return response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

    public function entrada(Request $request)
    {
        $temp = UserSystemInfoController::getusersysteminfo();
        //$temp['ip'] = '190.117.169.6';
        Log::info($temp);
        Log::info('Ingresando a entrada en AuthController');
        $credentials = $request->only('name', 'password');
        if (!Auth::attempt($credentials)) {
            Log::info('Error de Credenciales');
            return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
        }
        $user = $request->user();
        /* $asistencia = Assist::create(
            [
                'tipo' => 'entrada',
                'user_id' => $user->id,
                'device_id' => '1',
            ]
        ); */
        $device = Device::select('id')->where('ip',$temp['ip'])->first();
        Log::info($temp['ip']);
        Log::info($device);
        if(isset($device->id)){
            $assist = new Assist();
            $assist->tipo = 'entrada';
            $assist->user_id = $user->id;
            $assist->device_id = $device->id;
            $assist->save();
        }else{
            $assist = new Assist();
            $assist->tipo = 'entrada';
            $assist->user_id = $user->id;
            // $assist->device_id = $device->id;
            $assist->save();
        }

        

        $temp = response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
        Auth::guard('web')->logout();
    }

    public function salida(Request $request)
    {
        $temp = UserSystemInfoController::getusersysteminfo();
        Log::info($temp);
        Log::info('Ingresando a salida en AuthController');
        $credentials = $request->only('name', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        /* $asistencia = Assist::create(
            [
                'tipo' => 'salida',
                'user_id' => $user->id,
                'device_id' => '1',
            ]
        ); */
        $device = Device::select('id')->where('ip',$temp['ip'])->first();
        Log::info($temp['ip']);
        Log::info($device);
        if(isset($device->id)){
            $assist = new Assist();
            $assist->tipo = 'salida';
            $assist->user_id = $user->id;
            $assist->device_id = $device->id;
            $assist->save();
        }else{
            $assist = new Assist();
            $assist->tipo = 'salida';
            $assist->user_id = $user->id;
            // $assist->device_id = $device->id;
            $assist->save();
        }

        $temp = response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
        Auth::guard('web')->logout();
    }

}
