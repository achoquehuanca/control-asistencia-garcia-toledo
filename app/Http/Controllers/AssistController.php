<?php

namespace App\Http\Controllers;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Assist;
use App\Http\Controllers\UserSystemInfoController;
use App\Http\Controllers\Api\AuthController;
use Illuminate\Support\Facades\Log;
use \App\Laravue\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Laravue\Models\User;
use App\Device;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class AssistController extends Controller
{
    const ITEM_PER_PAGE = 15;

    /**
     * Display a listing of the user resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|ResourceCollection
     */
    public function index(Request $request)
    {
        $user = Auth::user()->id;
        Log::info($user);

        Log::info('Ingresando a Index de AssistController');
        Log::info($request);
        Log::info(isset($request['inicio']));
        Log::info(gettype($request->usuario));

        /*$query=Assist::with('user')->get();
        Log::info($query);*/

        if(isset($request->report)){
            if(isset($request->inicio)){
                if(isset($request->users)){
                    if(isset($request->sedes)){
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->whereBetween('assists.created_at',[$request->inicio, $request->fin])
                        ->whereIn('users.id',$request->users)
                        ->whereIn('devices.id',$request->sedes)
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }else{
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->whereBetween('assists.created_at',[$request->inicio, $request->fin])
                        ->whereIn('users.id',$request->users)
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }
                }else{
                    if(isset($request->sedes)){
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->whereBetween('assists.created_at',[$request->inicio, $request->fin])
                        ->whereIn('devices.id',$request->sedes)
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }else{
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->whereBetween('assists.created_at',[$request->inicio, $request->fin])
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }
                }
            }else{
                if(isset($request->users)){
                    if(isset($request->sedes)){
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->whereIn('users.id',$request->users)
                        ->whereIn('devices.id',$request->sedes)
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }else{
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->whereIn('users.id',$request->users)
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }
                }else{
                    if(isset($request->sedes)){
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->whereIn('devices.id',$request->sedes)
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }else{
                        $assists=\DB::table('assists')
                        ->leftjoin('users', 'assists.user_id', '=', 'users.id')
                        ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
                        ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
                        ->get();
                    }
                }
            }

            //$request['fin'] = Carbon::parse($request['fin'], 'America/Lima')->setTimezone('UTC');
            //$request['inicio'] = Carbon::parse($request['inicio'], 'America/Lima')->tz(config('app.timezone'));
            
            /*Log::info($request['inicio']);
            Log::info($request['fin']);
            $assists=\DB::table('assists')
            ->leftjoin('users', 'assists.user_id', '=', 'users.id')
            ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
            ->whereBetween('assists.created_at',[$request->inicio, $request->fin])
            ->whereIn('users.id',$request->users)
            ->whereIn('devices.id',$request->sedes)
            ->select('assists.id','users.nombres as nombres','users.paterno as paterno','users.materno as materno','tipo','assists.created_at','devices.sede as sede','devices.lugar as lugar')
            ->get();*/

            /*$assists = Assist::with(['user','device'])
            ->whereBetween('created_at',[$request->inicio, $request->fin])
            ->whereIn('user.id',$request->users)
            ->whereIn('device.id',$request->sedes)
            ->get();*/
            
        }else{
            if($user == 1)
            {
                $assists = Assist::with(['user','device'])
                ->get();
            }else{
                $assists = Assist::with(['user','device'])
                ->where('user_id',$user)
                ->get();
            }
        }

        Log::info($assists);
        $temp=response()->json(new JsonResponse(['items' => $assists, 'total' => mt_rand(1000, 10000)]));
        Log::info($temp);

        return $temp;


        /*$searchParams = $request->all();
        $userQuery = User::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $role = Arr::get($searchParams, 'role', '');
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($role)) {
            $userQuery->whereHas('roles', function($q) use ($role) { $q->where('name', $role); });
        }

        if (!empty($keyword)) {
            $userQuery->where('name', 'LIKE', '%' . $keyword . '%');
            $userQuery->where('email', 'LIKE', '%' . $keyword . '%');
        }

        return UserResource::collection($userQuery->paginate($limit));*/
    }
    public function load(Request $request)
    {
        $correctos = 0;
        $incorrectos = 0;
        // Log::info($request);
        foreach ($request->request as $key => $value) {
            Log::info($key);
            Log::info($value);
            $user = User::where('dni',$value['AC-No'])->first();
            Log::info($user);
            $asistencia = new Assist;
            $asistencia->created_at = strtr($value['Horario'],'/','-').':00';
            $asistencia->user_id = $user['id'];
            if(strstr($value['Estado'],'Ent')){
                $asistencia->tipo = 'entrada';
            }else {
                $asistencia->tipo = 'salida';
            }
            $guardado = $asistencia->save();
            if($guardado){
                $correctos++;
            }else{
                $incorrectos++;
            }
        }
        $response = 'Cargados = '.$correctos.', No cargados = '.$incorrectos;
        // return redirect()-back()->with('cargado','Cargados = '.$correctos.'No cargados = '.$correctos);
        return response()->json(['mensaje' => $response], Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials = $request->only('name', 'password');
        if (!Auth::attempt($credentials)) {
            Log::info('Ingresando a store de AssistController');
            return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
        }
        $user = $request->user();

        return response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);

        /*$params = $request->all();
        $user = Assist::create([
            'name' => $params['name'],
            'email' => $params['email'],
            'password' => Hash::make($params['password']),
        ]);
        $role = Role::findByName($params['role']);
        $user->syncRoles($role);

        return new UserResource($user);*/

        /*$validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
                [
                    'password' => ['required', 'min:6'],
                    'confirmPassword' => 'same:password',
                ]
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $params = $request->all();
            $user = User::create([
                'name' => $params['name'],
                'email' => $params['email'],
                'password' => Hash::make($params['password']),
            ]);
            $role = Role::findByName($params['role']);
            $user->syncRoles($role);

            return new UserResource($user);
        }*/

        
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User    $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        if ($user === null) {
            return response()->json(['error' => 'User not found'], 404);
        }
        if ($user->isAdmin()) {
            return response()->json(['error' => 'Admin can not be modified'], 403);
        }

        $currentUser = Auth::user();
        if (!$currentUser->isAdmin()
            && $currentUser->id !== $user->id
            && !$currentUser->hasPermission(\App\Laravue\Acl::PERMISSION_USER_MANAGE)
        ) {
            return response()->json(['error' => 'Permission denied'], 403);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $email = $request->get('email');
            $found = User::where('email', $email)->first();
            if ($found && $found->id !== $user->id) {
                return response()->json(['error' => 'Email has been taken'], 403);
            }

            $user->name = $request->get('name');
            $user->email = $email;
            $user->save();
            return new UserResource($user);
        }
    }
    public function destroy(User $user)
    {
        if ($user->isAdmin()) {
            return response()->json(['error' => 'Ehhh! Can not delete admin user'], 403);
        }

        try {
            $user->delete();
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }

        return response()->json(null, 204);
    }
    public function principal()
    {
        Log::info('Ingresando a principal');
        $inicio1 = Carbon::today();
        $fin1 = Carbon::today()->addDays(1);
        $inicio2 = Carbon::today()->subDays(1);
        $fin2 = Carbon::today();
        $inicio3 = Carbon::today()->subDays(2);
        $fin3 = Carbon::today()->subDays(1);
        $inicio4 = Carbon::today()->subDays(3);
        $fin4 = Carbon::today()->subDays(2);
        $inicio5 = Carbon::today()->subDays(4);
        $fin5 = Carbon::today()->subDays(3);
        $inicio6 = Carbon::today()->subDays(5);
        $fin6 = Carbon::today()->subDays(4);
        $inicio7 = Carbon::today()->subDays(6);
        $fin7 = Carbon::today()->subDays(5);
        //$dia1 = Assist::whereBetween(DB::raw('DATE(created_at)'), array($from_date, $to_date))->get();
        //$dia1 = Assist::whereBetween('created_at', [$from_date, $to_date])->get();
        $dia1 = DB::table('assists')
           ->whereBetween('created_at', [$inicio1, $fin1])
           ->select(\DB::raw("COUNT(*) as asistencia"))
           ->get();
        $dia2 = DB::table('assists')
           ->whereBetween('created_at', [$inicio2, $fin2])
           ->select(\DB::raw("COUNT(*) as asistencia"))
           ->get();
        $dia3 = DB::table('assists')
           ->whereBetween('created_at', [$inicio3, $fin3])
           ->select(\DB::raw("COUNT(*) as asistencia"))
           ->get();
        $dia4 = DB::table('assists')
           ->whereBetween('created_at', [$inicio4, $fin4])
           ->select(\DB::raw("COUNT(*) as asistencia"))
           ->get();
        $dia5 = DB::table('assists')
           ->whereBetween('created_at', [$inicio5, $fin5])
           ->select(\DB::raw("COUNT(*) as asistencia"))
           ->get();
        $dia6 = DB::table('assists')
           ->whereBetween('created_at', [$inicio6, $fin6])
           ->select(\DB::raw("COUNT(*) as asistencia"))
           ->get();
        $dia7 = DB::table('assists')
           ->whereBetween('created_at', [$inicio7, $fin7])
           ->select(\DB::raw("COUNT(*) as asistencia"))
           ->get();
        /*Log::info($dia1);
        Log::info($dia2);
        Log::info($dia3);
        Log::info($dia4);
        Log::info($dia5);
        Log::info($dia6);
        Log::info($dia7);*/

        $total = DB::table('users')
        ->select(\DB::raw("COUNT(*) as total"))
        ->get();
        
        /*Log::info(gettype($dia1));
        $asistencias = [];
        array_push($asistencias, $dia1);
        array_push($asistencias, $dia2);
        array_push($asistencias, $dia3);
        array_push($asistencias, $dia4);
        array_push($asistencias, $dia5);
        array_push($asistencias, $dia6);
        array_push($asistencias, $dia7);
        Log::info($asistencias);

        $total = [];
        array_push($total, $usuarios);
        Log::info($total);*/



        /*$inasistencias = [];
        array_push($inasistencias, $total);
        Log::info($total);*/

        $temp=response()->json(new JsonResponse([
            'dia1' => $dia1,
            'dia2' => $dia2,
            'dia3' => $dia3,
            'dia4' => $dia4,
            'dia5' => $dia5,
            'dia6' => $dia6,
            'dia7' => $dia7,
            'total' => $total]));
        Log::info($temp);

        /*$temp=response()->json(new JsonResponse([
            'dia1' => $dia1->pluck('asistencia1'),
            'dia2' => $dia2->pluck('asistencia2'),
            'dia3' => $dia3->pluck('asistencia3'),
            'dia4' => $dia4->pluck('asistencia4'),
            'dia5' => $dia5->pluck('asistencia5'),
            'dia6' => $dia6->pluck('asistencia6'),
            'dia7' => $dia7->pluck('asistencia7'),
            'total' => $total]));
        Log::info($temp);*/

        return $temp;

        //return response()->json(null, 204);
    }

    
    public function listar()
    {
        Log::info('Ingresando a listar');
        $userQuery = User::select('id', DB::raw("(CONCAT(dni, ' ', paterno, ' ', materno, ' ', nombres)) AS users"))
        ->get();
        Log::info($userQuery);
        return $userQuery;
    }

    public function listarSede()
    {
        Log::info('Ingresando a listarSede');
        $userQuery = Device::select('id', 'sede')
        ->get();
        Log::info($userQuery);
        return $userQuery;
    }

    public function listarAssist()
    {
        Log::info('Ingresando a listarAssist');
        $temp = UserSystemInfoController::getusersysteminfo();
        //$temp['ip'] = '190.117.169.6';
        $dispositivo = 0;
        $assists = [];
        Log::info($temp['ip']);
        if (Device::where('ip', 'like', '%' . $temp['ip'] . '%')->exists()) {
            $dispositivo = 1;
            $assists = \DB::table('assists')
            ->leftjoin('users', 'assists.user_id', '=', 'users.id')
            ->leftjoin('devices', 'assists.device_id', '=', 'devices.id')
            ->where('devices.ip',$temp['ip'])
            ->select('assists.created_at as date', DB::raw("(CONCAT(users.paterno, ' ', users.materno, ' ', users.nombres)) AS name"),'assists.tipo as address')
            ->orderBy('assists.created_at', 'desc')
            ->limit(5)
            ->get();
         }
        
        Log::info($assists);
        Log::info($dispositivo);
        $resultado = array('dispositivo' => $dispositivo, 'assists' => $assists);
        Log::info($resultado);
        return $resultado;
        /*$userQuery = Device::select('id', 'sede')
        ->get();
        Log::info($userQuery);
        return $userQuery;*/
    }
}
