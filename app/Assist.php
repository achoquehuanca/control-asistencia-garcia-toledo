<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assist extends Model
{

    protected $table = 'assists';

    protected $fillable = ['tipo', 'user_id', 'device_id'];

    public function user(){
        return $this->belongsTo('App\Laravue\Models\User', 'user_id', 'id');
    }
    public function device(){
        return $this->belongsTo('App\Device', 'device_id', 'id');
    }
}
