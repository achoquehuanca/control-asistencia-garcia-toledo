<?php

use App\Device;
use Illuminate\Database\Seeder;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* $dispositivo = Device::create(
            [
                'ip' => '179.6.164.47',
                'sede' => 'Central',
                'lugar' => 'Avenida Lima 777'
            ]
        ); */
        $dispositivo = Device::create(
            [
                'ip' => '181.67.2.253',
                'sede' => 'OFICINA BUSTAMANTE',
                'lugar' => 'Av. Daniel Alcides Carrion 269'
            ]
        );
        $dispositivo = Device::create(
            [
                'ip' => '190.117.169.6',
                'sede' => 'OFICINA CERRO COLORADO',
                'lugar' => 'Calle Miguel Grau N 216'
            ]
        );
        $dispositivo = Device::create(
            [
                'ip' => '190.117.78.62',
                'sede' => 'OFICINA GUARDIA CIVIL',
                'lugar' => 'Av. Pizarro 127'
            ]
        );
        $dispositivo = Device::create(
            [
                'ip' => '190.117.78.122',
                'sede' => 'OFICINA MIRAFLORES',
                'lugar' => 'Calle Manuel Muñoz Najar 410'
            ]
        );
        $dispositivo = Device::create(
            [
                'ip' => '161.132.127.139',
                'sede' => 'OFICINA PAMPITA ZEVALLOS',
                'lugar' => 'Calle Pampita Zevallos 175'
            ]
        );
        $dispositivo = Device::create(
            [
                'ip' => '179.6.164.206',
                'sede' => 'OFICINA PROGRESO',
                'lugar' => 'Av. Progreso 631'
            ]
        );
        $dispositivo = Device::create(
            [
                'ip' => '179.6.165.242',
                'sede' => 'OFICINA IV CENTENARIO',
                'lugar' => 'Calle Mayta Capac 601'
            ]
        );
        $dispositivo = Device::create(
            [
                'ip' => '179.6.164.112',
                'sede' => 'OFICINA PARRA',
                'lugar' => 'Av. Parra 388'
            ]
        );
    }
}
