<?php

use App\Laravue\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Laravue\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        /*$manager = User::create([
            'name' => 'Manager',
            'email' => 'manager@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        $editor = User::create([
            'name' => 'Editor',
            'email' => 'editor@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        $user = User::create([
            'name' => 'User',
            'email' => 'user@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        $visitor = User::create([
            'name' => 'Visitor',
            'email' => 'visitor@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);*/

        $employee001 = User::create([
            'name' => 'enparedes',
            'nombres' => 'EDITH NIEVES',
            'paterno' => 'PAREDES',
            'materno' => 'PERCA',
            'dni' => '40273542',
            'password' => Hash::make('40273542'),
        ]);
        $employee002 = User::create([
            'name' => 'hegarcia',
            'nombres' => 'HENRRY EMILIO',
            'paterno' => 'GARCIA',
            'materno' => 'FLORES',
            'dni' => '29396964',
            'password' => Hash::make('29396964'),
        ]);
        $employee003 = User::create([
            'name' => 'mctoledo',
            'nombres' => 'MARÍA CELIA',
            'paterno' => 'TOLEDO',
            'materno' => 'DE GARCÍA',
            'dni' => '29396963',
            'password' => Hash::make('29396963'),
        ]);
        $employee004 = User::create([
            'name' => 'smgarcia',
            'nombres' => 'SHARON MILUSKA',
            'paterno' => 'GARCIA',
            'materno' => 'TOLEDO',
            'dni' => '29673821',
            'password' => Hash::make('29673821'),
        ]);
        $employee005 = User::create([
            'name' => 'jmgarcia',
            'nombres' => 'JULISSA MAYENKA',
            'paterno' => 'GARCIA',
            'materno' => 'TOLEDO',
            'dni' => '41562425',
            'password' => Hash::make('41562425'),
        ]);
        $employee006 = User::create([
            'name' => 'sgarcia',
            'nombres' => 'STEPHANIE',
            'paterno' => 'GARCIA',
            'materno' => 'TOLEDO',
            'dni' => '70237342',
            'password' => Hash::make('70237342'),
        ]);
        $employee007 = User::create([
            'name' => 'lcgarcia',
            'nombres' => 'LOURDES CECILIA',
            'paterno' => 'GARCIA',
            'materno' => 'TOLEDO',
            'dni' => '70237343',
            'password' => Hash::make('70237343'),
        ]);
        $employee008 = User::create([
            'name' => 'narias',
            'nombres' => 'NAZARIO',
            'paterno' => 'ARIAS',
            'materno' => 'RAMOS',
            'dni' => '29550711',
            'password' => Hash::make('29550711'),
        ]);
        $employee009 = User::create([
            'name' => 'mjsurco',
            'nombres' => 'MARIO JAVIER',
            'paterno' => 'SURCO',
            'materno' => 'AROSTEGUI',
            'dni' => '29332637',
            'password' => Hash::make('29332637'),
        ]);
        $employee010 = User::create([
            'name' => 'mapinto',
            'nombres' => 'MANUEL ALEJANDRO',
            'paterno' => 'PINTO',
            'materno' => 'SILVA',
            'dni' => '41671238',
            'password' => Hash::make('41671238'),
        ]);
        $employee011 = User::create([
            'name' => 'rrtitto',
            'nombres' => 'RICHARD RUBEN',
            'paterno' => 'TITTO',
            'materno' => 'DEL CARPIO',
            'dni' => '29291490',
            'password' => Hash::make('29291490'),
        ]);
        $employee012 = User::create([
            'name' => 'rarce',
            'nombres' => 'RAFAEL',
            'paterno' => 'ARCE',
            'materno' => 'VALDIVIA',
            'dni' => '29600115',
            'password' => Hash::make('29600115'),
        ]);
        $employee013 = User::create([
            'name' => 'vdalvarez',
            'nombres' => 'VICTOR DAMIAN',
            'paterno' => 'ALVAREZ',
            'materno' => 'MAQUE',
            'dni' => '29402191',
            'password' => Hash::make('29402191'),
        ]);
        $employee014 = User::create([
            'name' => 'mecornejo',
            'nombres' => 'MARIA ELENA',
            'paterno' => 'CORNEJO',
            'materno' => 'BENGOA',
            'dni' => '29639655',
            'password' => Hash::make('29639655'),
        ]);
        $employee015 = User::create([
            'name' => 'jechalco',
            'nombres' => 'JEANNETE EMPERATRIZ',
            'paterno' => 'CHALCO',
            'materno' => 'MENDOZA',
            'dni' => '09396199',
            'password' => Hash::make('09396199'),
        ]);
        $employee016 = User::create([
            'name' => 'lquispe',
            'nombres' => 'LADISLAO',
            'paterno' => 'QUISPE',
            'materno' => 'QUISPE',
            'dni' => '29698249',
            'password' => Hash::make('29698249'),
        ]);
        $employee017 = User::create([
            'name' => 'reespinoza',
            'nombres' => 'REGNER ENRIQUE',
            'paterno' => 'ESPINOZA',
            'materno' => 'CUADROS',
            'dni' => '29428285',
            'password' => Hash::make('29428285'),
        ]);
        $employee018 = User::create([
            'name' => 'gpafernandez',
            'nombres' => 'GONZALO PABLO ANTONIO',
            'paterno' => 'FERNANDEZ',
            'materno' => 'URDAY',
            'dni' => '29306368',
            'password' => Hash::make('29306368'),
        ]);
        $employee019 = User::create([
            'name' => 'fvcoila',
            'nombres' => 'FREDY VILVER',
            'paterno' => 'COILA',
            'materno' => 'YANA',
            'dni' => '29470460',
            'password' => Hash::make('29470460'),
        ]);
        $employee020 = User::create([
            'name' => 'cmzuñiga',
            'nombres' => 'CLAUDIA MARLENE',
            'paterno' => 'ZUÑIGA',
            'materno' => 'HERRERA',
            'dni' => '29548805',
            'password' => Hash::make('29548805'),
        ]);
        $employee021 = User::create([
            'name' => 'rcastro',
            'nombres' => 'RAUL',
            'paterno' => 'CASTRO',
            'materno' => 'ARAGON',
            'dni' => '44958476',
            'password' => Hash::make('44958476'),
        ]);

        $employee022 = User::create([
            'name' => 'sslinares',
            'nombres' => 'STEPHANIE SANDRA',
            'paterno' => 'LINARES',
            'materno' => 'VELARDE',
            'dni' => '72185131',
            'password' => Hash::make('72185131'),
        ]);

        $employee023 = User::create([
            'name' => 'vlarico',
            'nombres' => 'VICENTINA',
            'paterno' => 'LARICO',
            'materno' => 'APAZA',
            'dni' => '29415186',
            'password' => Hash::make('29415186'),
        ]);

        $employee024 = User::create([
            'name' => 'rmportilla',
            'nombres' => 'RUBY MELISSA',
            'paterno' => 'PORTILLA',
            'materno' => 'MENDOZA',
            'dni' => '75480550',
            'password' => Hash::make('75480550'),
        ]);

        $employee025 = User::create([
            'name' => 'cvronceros',
            'nombres' => 'CHRISTOPHER VLADIMIR',
            'paterno' => 'RONCEROS',
            'materno' => 'CHALCO',
            'dni' => '72743632',
            'password' => Hash::make('72743632'),
        ]);

        $employee026 = User::create([
            'name' => 'ykescobar',
            'nombres' => 'YESSICA KARINA',
            'paterno' => 'ESCOBAR',
            'materno' => 'CALAHUILLE',
            'dni' => '43992892',
            'password' => Hash::make('43992892'),
        ]);

        $employee027 = User::create([
            'name' => 'vrbenavente',
            'nombres' => 'VICTOR REINALDO',
            'paterno' => 'BENAVENTE',
            'materno' => 'ALAYZA',
            'dni' => '04643745',
            'password' => Hash::make('04643745'),
        ]);

        $employee028 = User::create([
            'name' => 'vhrafael',
            'nombres' => 'VICTOR HUGO',
            'paterno' => 'RAFAEL',
            'materno' => 'RODRIGUEZ',
            'dni' => '29664212',
            'password' => Hash::make('29664212'),
        ]);

        $employee029 = User::create([
            'name' => 'rfcarrasco',
            'nombres' => 'RICARDO  FELIPE',
            'paterno' => 'CARRASCO',
            'materno' => 'RIVERA',
            'dni' => '29678199',
            'password' => Hash::make('29678199'),
        ]);

        $employee030 = User::create([
            'name' => 'rggarnica',
            'nombres' => 'ROXANA GABRIELA',
            'paterno' => 'GARNICA',
            'materno' => 'CUBA DE MELGAR',
            'dni' => '29314897',
            'password' => Hash::make('29314897'),
        ]);

        $employee031 = User::create([
            'name' => 'mapolanco',
            'nombres' => 'MIGUEL ANGEL',
            'paterno' => 'POLANCO',
            'materno' => 'SALAZAR',
            'dni' => '42851959',
            'password' => Hash::make('42851959'),
        ]);

        $employee032 = User::create([
            'name' => 'etvalencia',
            'nombres' => 'EDUARDO TOMAS',
            'paterno' => 'VALENCIA',
            'materno' => 'VALDEZ',
            'dni' => '43552623',
            'password' => Hash::make('43552623'),
        ]);

        $employee033 = User::create([
            'name' => 'yfhuayta',
            'nombres' => 'YESSENIA FIORELLA',
            'paterno' => 'HUAYTA',
            'materno' => 'QUISPE',
            'dni' => '45654346',
            'password' => Hash::make('45654346'),
        ]);

        $employee034 = User::create([
            'name' => 'jcmamani',
            'nombres' => 'JUAN CARLOS',
            'paterno' => 'MAMANI',
            'materno' => 'TEJEDA',
            'dni' => '41694379',
            'password' => Hash::make('41694379'),
        ]);

        $employee035 = User::create([
            'name' => 'nmflores',
            'nombres' => 'NOEMI MADELEINE',
            'paterno' => 'FLORES',
            'materno' => 'FLORES',
            'dni' => '41341844',
            'password' => Hash::make('41341844'),
        ]);

        $employee036 = User::create([
            'name' => 'baachata',
            'nombres' => 'BERTA ANDREA',
            'paterno' => 'ACHATA',
            'materno' => 'GALLEGOS',
            'dni' => '29399432',
            'password' => Hash::make('29399432'),
        ]);

        $employee037 = User::create([
            'name' => 'ghuahuachampi',
            'nombres' => 'GREGORIO',
            'paterno' => 'HUAHUACHAMPI',
            'materno' => 'PACHECO',
            'dni' => '29455030',
            'password' => Hash::make('29455030'),
        ]);

        $employee038 = User::create([
            'name' => 'jffzarate',
            'nombres' => 'JOHN FREDERICK FRANCISCO',
            'paterno' => 'ZARATE',
            'materno' => 'MACEDO',
            'dni' => '29550316',
            'password' => Hash::make('29550316'),
        ]);

        $employee039 = User::create([
            'name' => 'ngcamacho',
            'nombres' => 'NATALY GERALDINE',
            'paterno' => 'CAMACHO',
            'materno' => 'RIVEROS',
            'dni' => '47329698',
            'password' => Hash::make('47329698'),
        ]);

        $employee040 = User::create([
            'name' => 'acvelarde',
            'nombres' => 'ANANI CRISTINA',
            'paterno' => 'VELARDE',
            'materno' => 'CANO',
            'dni' => '29377902',
            'password' => Hash::make('29377902'),
        ]);

        $employee041 = User::create([
            'name' => 'hachavez',
            'nombres' => 'HEBERTH ALBERT',
            'paterno' => 'CHAVEZ',
            'materno' => 'HERRERA',
            'dni' => '44390984',
            'password' => Hash::make('44390984'),
        ]);

        $employee042 = User::create([
            'name' => 'zrzapana',
            'nombres' => 'ZOILA ROSA',
            'paterno' => 'ZAPANA',
            'materno' => 'QUISPE',
            'dni' => '29557195',
            'password' => Hash::make('29557195'),
        ]);

        $employee043 = User::create([
            'name' => 'mquispe',
            'nombres' => 'MARLENY',
            'paterno' => 'QUISPE',
            'materno' => 'ALCA',
            'dni' => '40157121',
            'password' => Hash::make('40157121'),
        ]);

        $employee044 = User::create([
            'name' => 'schambi',
            'nombres' => 'SARA',
            'paterno' => 'CHAMBI',
            'materno' => 'CUITO',
            'dni' => '29616779',
            'password' => Hash::make('29616779'),
        ]);

        $employee045 = User::create([
            'name' => 'oaquise',
            'nombres' => 'OLGA',
            'paterno' => 'AQUISE',
            'materno' => 'HUAMANI',
            'dni' => '76409471',
            'password' => Hash::make('76409471'),
        ]);

        $employee046 = User::create([
            'name' => 'emamani',
            'nombres' => 'ELISBAN',
            'paterno' => 'MAMANI',
            'materno' => 'CASTRO',
            'dni' => '40488024',
            'password' => Hash::make('40488024'),
        ]);

        $employee047 = User::create([
            'name' => 'crperez',
            'nombres' => 'CARMEN ROSA',
            'paterno' => 'PEREZ',
            'materno' => 'VELA DE LAZARINO',
            'dni' => '',
            'password' => Hash::make('29596082'),
        ]);

        $employee048 = User::create([
            'name' => 'pcondori',
            'nombres' => 'PATRICIA',
            'paterno' => 'CONDORI',
            'materno' => 'VILCA',
            'dni' => '47188217',
            'password' => Hash::make('47188217'),
        ]);
        $employee049 = User::create([
            'name' => 'amllerena',
            'nombres' => 'ALEXANDER MARTIN',
            'paterno' => 'LLERENA',
            'materno' => 'SILVA',
            'dni' => '72118393',
            'password' => Hash::make('72118393'),
        ]);

        $employee050 = User::create([
            'name' => 'jjberrios',
            'nombres' => 'JOSUE JONATHAN',
            'paterno' => 'BERRIOS',
            'materno' => 'ACHIRE',
            'dni' => '45788398',
            'password' => Hash::make('45788398'),
        ]);

        $employee051 = User::create([
            'name' => 'mmtrujillo',
            'nombres' => 'MIRIAM MICAELA',
            'paterno' => 'TRUJILLO',
            'materno' => 'ARCOS',
            'dni' => '40766278',
            'password' => Hash::make('40766278'),
        ]);

        $employee052 = User::create([
            'name' => 'awtorres',
            'nombres' => 'ARNALDO WILLIAM',
            'paterno' => 'TORRES',
            'materno' => 'BEJARANO',
            'dni' => '41644603',
            'password' => Hash::make('41644603'),
        ]);

        $employee053 = User::create([
            'name' => 'rancalla',
            'nombres' => 'RAUL',
            'paterno' => 'ANCALLA',
            'materno' => 'SANCHEZ',
            'dni' => '80201937',
            'password' => Hash::make('80201937'),
        ]);

        $employee054 = User::create([
            'name' => 'lmsrubio',
            'nombres' => 'LUISA MILAGROS SOLEDAD',
            'paterno' => 'RUBIO',
            'materno' => 'CASTAÑEDA',
            'dni' => '70418130',
            'password' => Hash::make('70418130'),
        ]);

        $employee055 = User::create([
            'name' => 'issuni',
            'nombres' => 'ISAUL SILVERIO',
            'paterno' => 'SUNI',
            'materno' => 'CONDORI',
            'dni' => '47305231',
            'password' => Hash::make('47305231'),
        ]);

        $employee056 = User::create([
            'name' => 'jlortiz',
            'nombres' => 'JOSE LUIS',
            'paterno' => 'ORTIZ',
            'materno' => 'LANDA',
            'dni' => '41849181',
            'password' => Hash::make('41849181'),
        ]);

        $employee057 = User::create([
            'name' => 'mpgarcia',
            'nombres' => 'MARÍA PATRICIA',
            'paterno' => 'GARCIA',
            'materno' => 'FLORES',
            'dni' => '29626457',
            'password' => Hash::make('29626457'),
        ]);

        $employee058 = User::create([
            'name' => 'svilca',
            'nombres' => 'SARA',
            'paterno' => 'VILCA',
            'materno' => 'PACHECHO',
            'dni' => '29737909',
            'password' => Hash::make('29737909'),
        ]);

        $employee059 = User::create([
            'name' => 'nmquispe',
            'nombres' => 'NATIVIDAD MARIA',
            'paterno' => 'QUISPE',
            'materno' => 'PACARA',
            'dni' => '29413915',
            'password' => Hash::make('29413915'),
        ]);

        $employee060 = User::create([
            'name' => 'nlmachaca',
            'nombres' => 'NICOLAS LEONCIO',
            'paterno' => 'MACHACA',
            'materno' => 'CHICCO',
            'dni' => '80199819',
            'password' => Hash::make('80199819'),
        ]);
        $employee061 = User::create([
            'name' => 'lrivera',
            'nombres' => 'LUISA',
            'paterno' => 'RIVERA',
            'materno' => 'CERVANTES',
            'dni' => '29716665',
            'password' => Hash::make('29716665'),
        ]);
        $employee062 = User::create([
            'name' => 'jllozada',
            'nombres' => 'JULIANA LSETH',
            'paterno' => 'LOZADA',
            'materno' => 'MARIN',
            'dni' => '29659544',
            'password' => Hash::make('29659544'),
        ]);
        $employee063 = User::create([
            'name' => 'alcalderon',
            'nombres' => 'ANDREA LUZ',
            'paterno' => 'CALDERON',
            'materno' => 'PARI',
            'dni' => '42767524',
            'password' => Hash::make('42767524'),
        ]);
        $employee064 = User::create([
            'name' => 'jveliz',
            'nombres' => 'JONATHAN',
            'paterno' => 'VELIZ',
            'materno' => 'QUICAÑO',
            'dni' => '47279869',
            'password' => Hash::make('47279869'),
        ]);
        $employee065 = User::create([
            'name' => 'rdpflores',
            'nombres' => 'ROSARIO DEL PILAR',
            'paterno' => 'FLORES ',
            'materno' => 'ACOSTA',
            'dni' => '80577237',
            'password' => Hash::make('80577237'),
        ]);
        $employee066 = User::create([
            'name' => 'macervantes',
            'nombres' => 'MELANIE ANDREA',
            'paterno' => 'CERVANTES',
            'materno' => 'NUÑEZ',
            'dni' => '71632152',
            'password' => Hash::make('71632152'),
        ]);
        $employee067 = User::create([
            'name' => 'nfarias',
            'nombres' => 'NOE FERNANDO',
            'paterno' => 'ARIAS',
            'materno' => 'COSI',
            'dni' => '48455558',
            'password' => Hash::make('48455558'),
        ]);
        $employee068 = User::create([
            'name' => 'ymzarate',
            'nombres' => 'YARIANNI MERCEDES',
            'paterno' => 'ZARATE',
            'materno' => 'MORALES',
            'dni' => '43316265',
            'password' => Hash::make('43316265'),
        ]);
        $employee069 = User::create([
            'name' => 'lyoblitas',
            'nombres' => 'LUZ YADINE',
            'paterno' => 'OBLITAS',
            'materno' => 'SIMON',
            'dni' => '43208538',
            'password' => Hash::make('43208538'),
        ]);
        $employee070 = User::create([
            'name' => 'gsgarcia',
            'nombres' => 'GABRIEL SEBASTIAN',
            'paterno' => 'GARCIA',
            'materno' => 'GARCIA',
            'dni' => '72805823',
            'password' => Hash::make('72805823'),
        ]);
        $employee071 = User::create([
            'name' => 'linuma',
            'nombres' => 'LLERLI',
            'paterno' => 'INUMA',
            'materno' => 'GUERRA',
            'dni' => '44571907',
            'password' => Hash::make('44571907'),
        ]);
        $employee072 = User::create([
            'name' => 'fapiminchumo',
            'nombres' => 'FLOR ANGELICA',
            'paterno' => 'PIMINCHUMO',
            'materno' => 'HINOSTROZA',
            'dni' => '41380895',
            'password' => Hash::make('41380895'),
        ]);
        $employee073 = User::create([
            'name' => 'drmedina',
            'nombres' => 'DALILA ROSA',
            'paterno' => 'MEDINA',
            'materno' => 'AROSQUIPA',
            'dni' => '73669073',
            'password' => Hash::make('73669073'),
        ]);
        $employee074 = User::create([
            'name' => 'amfarfan',
            'nombres' => 'ANA MARIA',
            'paterno' => 'FARFAN',
            'materno' => 'MOLINA',
            'dni' => '41027274',
            'password' => Hash::make('41027274'),
        ]);
        $employee075 = User::create([
            'name' => 'jegarcia',
            'nombres' => 'JONATHAN EMILIO',
            'paterno' => 'GARCIA',
            'materno' => 'TOLEDO',
            'dni' => '41187048',
            'password' => Hash::make('41187048'),
        ]);
        $employee076 = User::create([
            'name' => 'ahhidalgo',
            'nombres' => 'ALEXANDER HIDALGO',
            'paterno' => 'HIDALGO',
            'materno' => 'SOTO',
            'dni' => '44140975',
            'password' => Hash::make('44140975'),
        ]);
        $employee077 = User::create([
            'name' => 'kjgarcia',
            'nombres' => 'KAREN JANETH',
            'paterno' => 'GARCIA',
            'materno' => 'CHAPARRO',
            'dni' => '74698053',
            'password' => Hash::make('74698053'),
        ]);
        $employee078 = User::create([
            'name' => 'rrpelaez',
            'nombres' => 'RODOLFO RICARDO',
            'paterno' => 'PELAEZ',
            'materno' => 'BEJARANO',
            'dni' => '41571212',
            'password' => Hash::make('41571212'),
        ]);
        $employee079 = User::create([
            'name' => 'fstoledo',
            'nombres' => 'FERNANDO SANTOS',
            'paterno' => 'TOLEDO',
            'materno' => 'FERNANDEZ',
            'dni' => '29401569',
            'password' => Hash::make('29401569'),
        ]);
        $employee080 = User::create([
            'name' => 'kychavez',
            'nombres' => 'KARIN YANETH',
            'paterno' => 'CHAVEZ',
            'materno' => 'PINTO',
            'dni' => '42192157',
            'password' => Hash::make('42192157'),
        ]);
        $employee081 = User::create([
            'name' => 'napuma',
            'nombres' => 'NEREO AQUILES',
            'paterno' => 'PUMA',
            'materno' => 'CHITE',
            'dni' => '30661921',
            'password' => Hash::make('30661921'),
        ]);
        $employee082 = User::create([
            'name' => 'rmzeballos',
            'nombres' => 'ROSA MILAGROS',
            'paterno' => 'ZEVALLOS',
            'materno' => 'LARICANO',
            'dni' => '47668515',
            'password' => Hash::make('47668515'),
        ]);
        $employee083 = User::create([
            'name' => 'jjsalas',
            'nombres' => 'JOSTHIN JOWARD',
            'paterno' => 'SALAS',
            'materno' => 'FLORES',
            'dni' => '72727864',
            'password' => Hash::make('72727864'),
        ]);
        $employee084 = User::create([
            'name' => 'jbmamani',
            'nombres' => 'JORDAN BENITO',
            'paterno' => 'MAMANI',
            'materno' => 'HUAYHUA',
            'dni' => '70686457',
            'password' => Hash::make('70686457'),
        ]);
        $employee085 = User::create([
            'name' => 'egtaco',
            'nombres' => 'EVHERT GUSTAVO',
            'paterno' => 'TACO',
            'materno' => 'QUISPE ',
            'dni' => '44693228',
            'password' => Hash::make('44693228'),
        ]);
        $employee086 = User::create([
            'name' => 'jplopez',
            'nombres' => 'JOSEPH PAUL',
            'paterno' => 'LOPEZ',
            'materno' => 'DEL CARPIO',
            'dni' => '46780353',
            'password' => Hash::make('46780353'),
        ]);
        $employee087 = User::create([
            'name' => 'jmmontes',
            'nombres' => 'JHEAN MARCO',
            'paterno' => 'MONTES',
            'materno' => 'MONTES',
            'dni' => '47545869',
            'password' => Hash::make('47545869'),
        ]);
        $employee088 = User::create([
            'name' => 'dmprado',
            'nombres' => 'DALI MABEL',
            'paterno' => 'PRADO',
            'materno' => 'GUZMAN',
            'dni' => '43773211',
            'password' => Hash::make('43773211'),
        ]);
        $employee089 = User::create([
            'name' => 'pbsalomon',
            'nombres' => 'PATRICIA BRENDA',
            'paterno' => 'SALOMON',
            'materno' => 'NUÑEZ',
            'dni' => '70005564',
            'password' => Hash::make('70005564'),
        ]);
        $employee090 = User::create([
            'name' => 'mdcyañez',
            'nombres' => 'MARIA DEL CARMEN',
            'paterno' => 'YAÑEZ',
            'materno' => 'YAÑEZ DE CANO',
            'dni' => '29709234',
            'password' => Hash::make('29709234'),
        ]);
        $employee091 = User::create([
            'name' => 'gmamani',
            'nombres' => 'GERMAN',
            'paterno' => 'MAMANI',
            'materno' => 'FLORES',
            'dni' => '44655435',
            'password' => Hash::make('44655435'),
        ]);
        $employee092 = User::create([
            'name' => 'djlajo',
            'nombres' => 'DANIEL JAIME',
            'paterno' => 'LAJO',
            'materno' => 'ROJAS',
            'dni' => '70479699',
            'password' => Hash::make('70479699'),
        ]);
        $employee093 = User::create([
            'name' => 'hrgomez',
            'nombres' => 'HUGO RENE',
            'paterno' => 'GOMEZ',
            'materno' => 'SIERRA',
            'dni' => '001702125',
            'password' => Hash::make('001702125'),
        ]);
        $employee094 = User::create([
            'name' => 'aalejo',
            'nombres' => 'ANTONIO',
            'paterno' => 'ALEJO',
            'materno' => 'MAMANI',
            'dni' => '29676612',
            'password' => Hash::make('29676612'),
        ]);
        $employee095 = User::create([
            'name' => 'gmelgar',
            'nombres' => 'GONZALO',
            'paterno' => 'MELGAR',
            'materno' => 'PERALTA',
            'dni' => '29422073',
            'password' => Hash::make('29422073'),
        ]);
        $employee096 = User::create([
            'name' => 'aemorales',
            'nombres' => 'ALVARO ENRIQUE',
            'paterno' => 'MORALES',
            'materno' => 'ARCE',
            'dni' => '29409559',
            'password' => Hash::make('29409559'),
        ]);
        $employee097 = User::create([
            'name' => 'fdmhinojosa',
            'nombres' => 'FLOR DE MARIA',
            'paterno' => 'HINOJOSA ',
            'materno' => 'MARROQUIN',
            'dni' => '70355498',
            'password' => Hash::make('70355498'),
        ]);
        $employee098 = User::create([
            'name' => 'nscruz',
            'nombres' => 'NARELA STEFANY',
            'paterno' => 'CRUZ',
            'materno' => 'ALVAREZ',
            'dni' => '71642280',
            'password' => Hash::make('71642280'),
        ]);
        $employee099 = User::create([
            'name' => 'pcyugar',
            'nombres' => 'PATRICIA CAROLINA',
            'paterno' => 'YUGAR',
            'materno' => 'LINARES',
            'dni' => '47261741',
            'password' => Hash::make('47261741'),
        ]);
        $employee100 = User::create([
            'name' => 'hsabina',
            'nombres' => 'HUGO',
            'paterno' => 'SABINA',
            'materno' => 'SONIGA',
            'dni' => '46647211',
            'password' => Hash::make('46647211'),
        ]);
        $employee101 = User::create([
            'name' => 'ecquispe',
            'nombres' => 'ELIZABETH CARMEN',
            'paterno' => 'QUISPE',
            'materno' => 'SANCA',
            'dni' => '71330001',
            'password' => Hash::make('71330001'),
        ]);
        $employee102 = User::create([
            'name' => 'fsnuñez',
            'nombres' => 'FELIPE STEWART',
            'paterno' => 'NUÑEZ',
            'materno' => 'OLARTE',
            'dni' => '43189654',
            'password' => Hash::make('43189654'),
        ]);
        $employee103 = User::create([
            'name' => 'vanuñez',
            'nombres' => 'VALENTIN ABRAM',
            'paterno' => 'NUÑEZ',
            'materno' => 'OLARTE',
            'dni' => '43384166',
            'password' => Hash::make('43384166'),
        ]);
        $employee104 = User::create([
            'name' => 'cmportillo',
            'nombres' => 'CIRO MARTIN',
            'paterno' => 'PORTILLO',
            'materno' => 'ROMERO',
            'dni' => '75495416',
            'password' => Hash::make('75495416'),
        ]);
        $employee105 = User::create([
            'name' => 'ajquispe',
            'nombres' => 'ALVARO JOSE',
            'paterno' => 'QUISPE',
            'materno' => 'PACORI',
            'dni' => '47871391',
            'password' => Hash::make('47871391'),
        ]);
        $employee106 = User::create([
            'name' => 'rcahuapaza',
            'nombres' => 'RICHARD',
            'paterno' => 'CAHUAPAZA',
            'materno' => 'COTRADO',
            'dni' => '29416589',
            'password' => Hash::make('29416589'),
        ]);
        $employee107 = User::create([
            'name' => 'lhuayhua',
            'nombres' => 'LISBET',
            'paterno' => 'HUAYHUA',
            'materno' => 'SALHUA',
            'dni' => '75378959',
            'password' => Hash::make('75378959'),
        ]);
        $employee108 = User::create([
            'name' => 'ldhuamani',
            'nombres' => 'LUIS DAVID',
            'paterno' => 'HUAMANI',
            'materno' => 'CCAHUA',
            'dni' => '72171077',
            'password' => Hash::make('72171077'),
        ]);
        $employee109 = User::create([
            'name' => 'arcama',
            'nombres' => 'ARNOLD RONALD',
            'paterno' => 'CAMA',
            'materno' => 'ROJAS',
            'dni' => '43761015',
            'password' => Hash::make('43761015'),
        ]);
        $employee110 = User::create([
            'name' => 'wfzavala',
            'nombres' => 'WILLIAM FRANCO',
            'paterno' => 'ZAVALA',
            'materno' => 'CARDENAS',
            'dni' => '76071265',
            'password' => Hash::make('76071265'),
        ]);
        $employee111 = User::create([
            'name' => 'afmendoza',
            'nombres' => 'ABEL ALFONSO',
            'paterno' => 'MENDOZA',
            'materno' => 'PALOMINO',
            'dni' => '44397586',
            'password' => Hash::make('44397586'),
        ]);
        $employee112 = User::create([
            'name' => 'abastidas',
            'nombres' => 'ALFREDO',
            'paterno' => 'BASTIDAS',
            'materno' => 'SANCHEZ',
            'dni' => '76142347',
            'password' => Hash::make('76142347'),
        ]);
        $employee113 = User::create([
            'name' => 'mrticona',
            'nombres' => 'MERLING RAUL',
            'paterno' => 'TICONA',
            'materno' => 'QUISPE',
            'dni' => '70600571',
            'password' => Hash::make('70600571'),
        ]);
        $employee114 = User::create([
            'name' => 'aparosquipa',
            'nombres' => 'ANA PAOLA',
            'paterno' => 'AROSQUIPA',
            'materno' => 'PAREDES',
            'dni' => '73249359',
            'password' => Hash::make('73249359'),
        ]);
        $employee115 = User::create([
            'name' => 'jljara',
            'nombres' => 'JULIAN LUIS',
            'paterno' => 'JARA',
            'materno' => 'HUANCA',
            'dni' => '73047689',
            'password' => Hash::make('73047689'),
        ]);
        $employee116 = User::create([
            'name' => 'jcquintana',
            'nombres' => 'JONATHAN CHRISTOPHER',
            'paterno' => 'QUINTANA',
            'materno' => 'LLERENA',
            'dni' => '42333414',
            'password' => Hash::make('42333414'),
        ]);
        $employee117 = User::create([
            'name' => 'zemamani',
            'nombres' => 'ZENAIDA EVA',
            'paterno' => 'MAMANI',
            'materno' => 'PARILLO',
            'dni' => '76081394',
            'password' => Hash::make('76081394'),
        ]);
        $employee118 = User::create([
            'name' => 'gmvargas',
            'nombres' => 'GLENY MELANY',
            'paterno' => 'VARGAS',
            'materno' => 'MORON',
            'dni' => '47265422',
            'password' => Hash::make('47265422'),
        ]);
        $employee119 = User::create([
            'name' => 'aegonzales',
            'nombres' => 'ALVARO ALEJANDRO',
            'paterno' => 'GONZALES',
            'materno' => 'CALVO',
            'dni' => '47293125',
            'password' => Hash::make('47293125'),
        ]);
        $employee120 = User::create([
            'name' => 'eamamani',
            'nombres' => 'ELOY ANGEL',
            'paterno' => 'MAMANI',
            'materno' => 'RUIZ',
            'dni' => '29724523',
            'password' => Hash::make('29724523'),
        ]);
        $employee121 = User::create([
            'name' => 'omtanco',
            'nombres' => 'ORIANA MEGAN',
            'paterno' => 'TANCO',
            'materno' => 'GARCIA',
            'dni' => '75415139',
            'password' => Hash::make('75415139'),
        ]);
        $employee122 = User::create([
            'name' => 'pgarcia',
            'nombres' => 'PETTER',
            'paterno' => 'GARCIA',
            'materno' => 'BUSTINZA',
            'dni' => '42892408',
            'password' => Hash::make('42892408'),
        ]);
        $employee123 = User::create([
            'name' => 'iaflores',
            'nombres' => 'ISMENIA ARACELY',
            'paterno' => 'FLORES ',
            'materno' => 'GARCIA',
            'dni' => '72214661',
            'password' => Hash::make('72214661'),
        ]);
        $employee124 = User::create([
            'name' => 'gfmontoya',
            'nombres' => 'GINA FAVIOLA',
            'paterno' => 'MONTOYA',
            'materno' => 'BERNAL',
            'dni' => '43727288',
            'password' => Hash::make('43727288'),
        ]);


        $adminRole = Role::findByName(\App\Laravue\Acl::ROLE_ADMIN);
        $managerRole = Role::findByName(\App\Laravue\Acl::ROLE_MANAGER);
        $editorRole = Role::findByName(\App\Laravue\Acl::ROLE_EDITOR);
        $userRole = Role::findByName(\App\Laravue\Acl::ROLE_USER);
        $visitorRole = Role::findByName(\App\Laravue\Acl::ROLE_VISITOR);
        $admin->syncRoles($adminRole);
        /*$manager->syncRoles($managerRole);
        $editor->syncRoles($editorRole);
        $user->syncRoles($userRole);
        $visitor->syncRoles($visitorRole);*/
        $employee001->syncRoles($userRole);
        $employee002->syncRoles($userRole);
        $employee003->syncRoles($userRole);
        $employee004->syncRoles($userRole);
        $employee005->syncRoles($userRole);
        $employee006->syncRoles($userRole);
        $employee007->syncRoles($userRole);
        $employee008->syncRoles($userRole);
        $employee009->syncRoles($userRole);
        $employee010->syncRoles($userRole);
        $employee011->syncRoles($userRole);
        $employee012->syncRoles($userRole);
        $employee013->syncRoles($userRole);
        $employee014->syncRoles($userRole);
        $employee015->syncRoles($userRole);
        $employee016->syncRoles($userRole);
        $employee017->syncRoles($userRole);
        $employee018->syncRoles($userRole);
        $employee019->syncRoles($userRole);
        $employee020->syncRoles($userRole);
        $employee021->syncRoles($userRole);
        $employee022->syncRoles($userRole);
        $employee023->syncRoles($userRole);
        $employee024->syncRoles($userRole);
        $employee025->syncRoles($userRole);
        $employee026->syncRoles($userRole);
        $employee027->syncRoles($userRole);
        $employee028->syncRoles($userRole);
        $employee029->syncRoles($userRole);
        $employee030->syncRoles($userRole);
        $employee031->syncRoles($userRole);
        $employee032->syncRoles($userRole);
        $employee033->syncRoles($userRole);
        $employee034->syncRoles($userRole);
        $employee035->syncRoles($userRole);
        $employee036->syncRoles($userRole);
        $employee037->syncRoles($userRole);
        $employee038->syncRoles($userRole);
        $employee039->syncRoles($userRole);
        $employee040->syncRoles($userRole);
        $employee041->syncRoles($userRole);
        $employee042->syncRoles($userRole);
        $employee043->syncRoles($userRole);
        $employee044->syncRoles($userRole);
        $employee045->syncRoles($userRole);
        $employee046->syncRoles($userRole);
        $employee047->syncRoles($userRole);
        $employee048->syncRoles($userRole);
        $employee049->syncRoles($userRole);
        $employee050->syncRoles($userRole);
        $employee051->syncRoles($userRole);
        $employee052->syncRoles($userRole);
        $employee053->syncRoles($userRole);
        $employee054->syncRoles($userRole);
        $employee055->syncRoles($userRole);
        $employee056->syncRoles($userRole);
        $employee057->syncRoles($userRole);
        $employee058->syncRoles($userRole);
        $employee059->syncRoles($userRole);
        $employee060->syncRoles($userRole);
        $employee061->syncRoles($userRole);
        $employee062->syncRoles($userRole);
        $employee063->syncRoles($userRole);
        $employee064->syncRoles($userRole);
        $employee065->syncRoles($userRole);
        $employee066->syncRoles($userRole);
        $employee067->syncRoles($userRole);
        $employee068->syncRoles($userRole);
        $employee069->syncRoles($userRole);
        $employee070->syncRoles($userRole);
        $employee071->syncRoles($userRole);
        $employee072->syncRoles($userRole);
        $employee073->syncRoles($userRole);
        $employee074->syncRoles($userRole);
        $employee075->syncRoles($userRole);
        $employee076->syncRoles($userRole);
        $employee077->syncRoles($userRole);
        $employee078->syncRoles($userRole);
        $employee079->syncRoles($userRole);
        $employee080->syncRoles($userRole);
        $employee081->syncRoles($userRole);
        $employee082->syncRoles($userRole);
        $employee083->syncRoles($userRole);
        $employee084->syncRoles($userRole);
        $employee085->syncRoles($userRole);
        $employee086->syncRoles($userRole);
        $employee087->syncRoles($userRole);
        $employee088->syncRoles($userRole);
        $employee089->syncRoles($userRole);
        $employee090->syncRoles($userRole);
        $employee091->syncRoles($userRole);
        $employee092->syncRoles($userRole);
        $employee093->syncRoles($userRole);
        $employee094->syncRoles($userRole);
        $employee095->syncRoles($userRole);
        $employee096->syncRoles($userRole);
        $employee097->syncRoles($userRole);
        $employee098->syncRoles($userRole);
        $employee099->syncRoles($userRole);
        $employee100->syncRoles($userRole);
        $employee101->syncRoles($userRole);
        $employee102->syncRoles($userRole);
        $employee103->syncRoles($userRole);
        $employee104->syncRoles($userRole);
        $employee105->syncRoles($userRole);
        $employee106->syncRoles($userRole);
        $employee107->syncRoles($userRole);
        $employee108->syncRoles($userRole);
        $employee109->syncRoles($userRole);
        $employee110->syncRoles($userRole);
        $employee111->syncRoles($userRole);
        $employee112->syncRoles($userRole);
        $employee113->syncRoles($userRole);
        $employee114->syncRoles($userRole);
        $employee115->syncRoles($userRole);
        $employee116->syncRoles($userRole);
        $employee117->syncRoles($userRole);
        $employee118->syncRoles($userRole);
        $employee119->syncRoles($userRole);
        $employee120->syncRoles($userRole);
        $employee121->syncRoles($userRole);
        $employee122->syncRoles($userRole);
        $employee123->syncRoles($userRole);
        $employee124->syncRoles($userRole);
        
        $this->call(UsersTableSeeder::class);
        $this->call(DeviceSeeder::class);
        $this->call(AssistSeeder::class);
        $this->call(DocumentSeeder::class);
    }
}
