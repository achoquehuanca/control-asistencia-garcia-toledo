/* import request from '@/utils/request';
import Resource from '@/api/resource';

class UserResource extends Resource {
  constructor() {
    super('assists');
  }

  permissions(id) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'get',
    });
  }

  updatePermission(id, permissions) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'put',
      data: permissions,
    });
  }
}

export { UserResource as default }; */
import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/assist',
    method: 'get',
    params: query,
  });
}

export function fetchAssist(id) {
  return request({
    url: '/assist/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/assist/' + id + '/pageviews',
    method: 'get',
  });
}

export function createAssist(data) {
  return request({
    url: '/assist/create',
    method: 'post',
    data,
  });
}

export function loadAssists(data) {
  return request({
    url: '/assist/load',
    method: 'post',
    data,
  });
}

export function updateAssist(data) {
  return request({
    url: '/assist/update',
    method: 'post',
    data,
  });
}

export function fetchPanel() {
  return request({
    url: '/assist/panel',
    method: 'get',
  });
}

export function listar() {
  return request({
    url: '/user/list',
    method: 'get',
  });
}
export function listarSede() {
  return request({
    url: '/sede/list',
    method: 'get',
  });
}
export function listarAssist() {
  return request({
    url: '/login/list',
    method: 'get',
  });
}

