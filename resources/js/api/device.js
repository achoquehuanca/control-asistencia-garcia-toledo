import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/device',
    method: 'get',
    params: query,
  });
}

export function fetchDevice(id) {
  return request({
    url: '/device/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/device/' + id + '/pageviews',
    method: 'get',
  });
}

export function createDevice(data) {
  return request({
    url: '/device/create',
    method: 'post',
    data,
  });
}

export function updateDevice(data) {
  return request({
    url: '/device/update',
    method: 'post',
    data,
  });
}
