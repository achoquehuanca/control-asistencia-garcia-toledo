import request from '@/utils/request';

export function login(data) {
  console.log('Login');
  return request({
    url: '/auth/login',
    method: 'post',
    data: data,
  });
}

export function getInfo(token) {
  return request({
    url: '/user',
    method: 'get',
  });
}

export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post',
  });
}

export function csrf() {
  console.log('Agregando ruta csrf');
  return request({
    url: '/sanctum/csrf-cookie',
    method: 'get',
  });
}

export function entrada(data) {
  console.log('redirigiendo a entrada en resource/js/api/auth.js');
  console.log(data);
  return request({
    url: '/auth/entrada',
    method: 'post',
    data: data,
  });
}

export function salida(data) {
  console.log('redirigiendo a salida en resource/js/api/auth.js');
  return request({
    url: '/auth/salida',
    method: 'post',
    data: data,
  });
}
